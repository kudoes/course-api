package io.javabrains.springbootstarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// The following annotation tells Spring Boot that this file is the starting point for our spring boot application
@SpringBootApplication
public class CourseApiApp {

	public static void main(String[] args) {
		// What we need to do:
		// Start the application
		// Create a servlet container
		// Host that app in that servlet container
		// Make it available

		// This can all be done through this one method
		SpringApplication.run(CourseApiApp.class, args);

	}

}
